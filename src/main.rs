#[allow(unused_doc_comments)]

use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;
use std::collections::HashMap;
use std::env;

#[derive(Debug)]
struct Row {
  date: Date,
  message: String,
}

#[derive(Debug)]
struct Date {
  day: String,
  month: String,
  year: String
}

impl Date {
  fn parse(&mut self, date: &String) -> () {
    let converter: String = date.to_string();
    let parts: Vec<&str> = converter.split("/").collect();
    self.day = parts[0].to_string();
    self.month = parts[1].to_string();
    self.year = parts[2].to_string();
  }
}

fn main() -> std::io::Result<()> {
    let args: Vec<String> = env::args().collect();

    let f = File::open(&args[1])?;
    let mut reader = BufReader::new(f);

    let mut line = String::new();
    let mut len = reader.read_line(&mut line)?;
    let mut messages = HashMap::new();

    /// Counters for lines processed and lines skipped during parsing
    let mut lc = 0;
    let mut ls = 0;

    while len != 0 {
      {
        let row: Vec<&str> = line.split(',').collect();
        let pt: String = row[0].chars().take(1).collect();
        let sc: String = row[0].chars().skip(1).take(1).collect();

        if row[0].len() < 6 {
            line.clear();
            ls = ls + 1;
            len = reader.read_line(&mut line)?;
            continue;
        }

        let mut skipper: usize = 1;
        if sc == "[" {
          skipper = 2;
        }

        if pt == "[" || sc == "[" {
          let date: String = row[0].chars().skip(skipper).take(row[0].len()).collect();
          let mut ds: Date = Date{day: String::from(""), month: String::from(""), year: String::from("")};
          ds.parse(&date);

          let _temp_row: Row = Row{message: row[1].to_string(), date: ds};

          if messages.contains_key(&date) {
            if let Some(x) = messages.get_mut(&date) {
              *x = *x + 1;
            }
          } else {
            messages.insert(date, 1);
          }
        }
      }

      line.clear();
      lc = lc + 1;
      len = reader.read_line(&mut line)?;
    }

    println!("{0} lines processed.\n", lc);
    println!("{0} lines skipped.\n", ls);
    println!("{:?}", messages);

    Ok(())
}